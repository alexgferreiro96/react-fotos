import React, { useEffect, useState } from "react";
import { Button, Row, Col, Container } from "reactstrap";
import { v4 as uuid } from "uuid";
import OneItem from "./OneItem";

const Items = () => {
  const [items, setItems] = useState([]);
  const [url, setUrl] = useState();


  const guardar = () => {
    localStorage.setItem('mis_items', JSON.stringify(items));
  }

  const recuperar = () => {
    const itemsJson = localStorage.getItem('mis_items');
    const cosas = JSON.parse(itemsJson);
    if (cosas && cosas.length){
      setItems(cosas);
    } else {
      setItems([]);
    }

  }

  const afegir = () => {
    if(url){
      const nouItem = {
        imagen: url,
        id: uuid(),
        likes: 0
      };
      setItems( [...items, nouItem] );
      setUrl('');
    }
     
  };

  const addLikes = (id) => {
      let arrayItemsCopy = items.map(it => {
        if(it.id === id) it.likes++;
        return it;
      });
      setItems(arrayItemsCopy);
  }

  const tots = items.map((el, index) => (
    <Col><OneItem item={el} addLike={addLikes} key={index+"-"+el.id} index={index} /></Col>
  ));

  useEffect(() => {
      recuperar();
  }, []);

  return (
    <Container fluid className={"mainContainer"}>
        <h1 className="titleApp">WallaXof</h1>
        <input type="text" value={url} onChange={(ev) => setUrl(ev.target.value)} placeholder={"Ponga su url caballero ..."} />
        <br />
        <Button color={"primary"} onClick={afegir}>Afegir</Button>
        <Button color={"primary"} onClick={guardar}>Guardar datos</Button>
        <br />
        <br />
        <Row xs={5}>
            {tots}
        </Row>
    </Container>
  );
};

export default Items;