import { Card, CardBody, Container,Button, CardImg } from "reactstrap";

const OneItem = (props) => {
    return(
        <Card>
            <CardImg top width="100%" src={props.item.imagen} alt={props.item.id} />
            <CardBody>
                <Container>
                    <Button color={'danger'} onClick={()=>props.addLike(props.item.id)}>Like</Button>   {props.item.likes}
                </Container>
            </CardBody>
        </Card>
    )
}

export default OneItem;